import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Home from './Home'
import FooterHome from './FooterHome'
import NovoAnuncio from './NovoAnuncio'
import Categorias from './Categorias'
import base from './base'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      categorias: []
    }

    base.bindToState('categorias', {
      context: this,
      state: 'categorias'
    })
  }

  render() {
    return (
      <Router>
        <div className="App">

          <Route exact path='/' render={() => {
            return (
              <Home categorias={this.state.categorias} />
            )
          }} />

          <Route exact path='/novo-anuncio' render={() => {
            return (
              <NovoAnuncio categorias={this.state.categorias} />
            )
          }} />

          <Route path='/categorias' render={() => {
            return (
              <Categorias categorias={this.state.categorias} />
            )
          }} />

          <FooterHome />
        </div>
      </Router>
    );
  }
}

export default App;
