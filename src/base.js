const config = {
    apiKey: "AIzaSyDUC-ptLqe9Si_qK8CGlAsLrWS_5EzQUG4",
    authDomain: "mercadodev-50271.firebaseapp.com",
    databaseURL: "https://mercadodev-50271.firebaseio.com",
    projectId: "mercadodev-50271",
    storageBucket: "gs://mercadodev-50271.appspot.com/",
    messagingSenderId: "457311737814"
};

const rebase = require('re-base')
const firebase = require('firebase/app')
require('firebase/database')
require('firebase/storage')

const app = firebase.initializeApp(config)
const base = rebase.createClass(app.database())

export const storage = app.storage()

export default base