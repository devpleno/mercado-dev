import React, { Component } from 'react'
import base from './base'
import LinkCategorias from './LinkCategorias'
import AnuncioHome from './AnuncioHome'
import HeaderHome from './HeaderHome'

class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {
            anuncios: []
        }

        base.bindToState('anuncios', {
            context: this,
            state: 'anuncios',
            queries: {
                limitToLast: 3
            }
        })
    }

    render() {

        let indexC = 0
        let indexA = 0

        return (
            <div>
                <HeaderHome />

                <div className="container">
                    <h3>Ultimos anuncios</h3>

                        <div className="row">
                            {Object.keys(this.state.anuncios).map((key, i) => {
                                const res = this.state.anuncios[key]

                                return [
                                    <AnuncioHome key={'anu-' + i} anuncio={res} id={key} />,
                                    ++indexA % 4 === 0 && <div key={'anu-div-' + i} className="w-100"></div>
                                ]
                            }
                            )}
                        </div>

                        <div className="row">
                            {Object.keys(this.props.categorias).map((key, i) => {
                                const res = this.props.categorias[key]

                                return [
                                    <LinkCategorias key={'cat-' + i} categoria={{ titulo: res.categoria, url: res.url }} icone={res.icon} />,
                                    ++indexC % 4 === 0 && <div key={'cat-div-' + i} className="w-100"></div>
                                ]
                            }
                            )}
                        </div>
                </div>
            </div>
        )
    }
}

export default Home