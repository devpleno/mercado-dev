import React, { Component } from 'react'
import axios from 'axios'

class Anuncio extends Component {
    constructor(props) {
        super(props)

        this.state = {
            anuncio: {},
            isLoading: true
        }

        const id = this.props.match.params.idAnuncio

        axios.get('https://mercadodev-50271.firebaseio.com/anuncios/' + id + '.json').then((res) => {
            this.setState({ anuncio: res['data'], isLoading: false })
        })
    }

    render() {
        const anuncio = this.state.anuncio

        if(this.state.isLoading) {
            return <i className='fa fa-circle-o-notch fa-spin fa-3x fa-fw'></i>
        }

        return (
            <div>
                <h2>Anuncio</h2>
                <p><img src={anuncio.foto} width='500' /></p>
                <p>Nome: {anuncio.nome}</p>
                <p>Descrição: {anuncio.descricao}</p>
            </div>
        )
    }
}

export default Anuncio