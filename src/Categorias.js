import React from 'react'
import { Link, Route } from 'react-router-dom'
import HeaderInterno from './HeaderInterno';
import Categoria from './Categoria';
import Anuncio from './Anuncio';

const Categorias = (props) => {
    return (
        <div>
            <HeaderInterno />

            <div className="container" style={{ paddingTop: '120px' }}>
                <h2>Categorias</h2>

                <div className="row">
                    <div className="col-lg-4">
                        <ul>
                            {Object.keys(props.categorias).map((key, i) => {
                                const cat = props.categorias[key]

                                return (
                                    <li key={'cat-' + i}>
                                        <Link to={'/categorias/' + cat.url}>{cat.categoria}</Link>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>

                    <div className="col-lg-8">
                        <Route path='/categorias/:urlCat' exact component={Categoria} />
                        <Route path='/categorias/:urlCat/:idAnuncio' component={Anuncio} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Categorias