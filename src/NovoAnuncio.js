import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import HeaderInterno from './HeaderInterno'
import base, { storage } from './base'

class NovoAnuncio extends Component {

    constructor(props) {
        super(props)

        this.state = {
            success: false
        }

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(ev) {

        const foto = this.foto.files[0]
        const { name } = foto

        const ref = storage.ref(name)

        ref.put(foto).then((img) => {
            const fName = img.metadata.downloadURLs[0]

            const novoAnuncio = {
                nome: this.nome.value,
                descricao: this.descricao.value,
                categoria: this.categoria.value,
                preco: this.preco.value,
                telefone: this.telefone.value,
                vendedor: this.vendedor.value,
                foto: fName
            }

            base.push('anuncios', {
                data: novoAnuncio
            }).then(() => {
                this.setState({ success: true })
            }
            )
        })

        ev.preventDefault()
    }

    render() {

        // if (this.state.success) {
        //     return <Redirect to='/' />
        // }

        return (
            <div>

                {this.state.success && <Redirect to='/' />}
                <HeaderInterno />

                <div className="container" style={{ paddingTop: '120px' }}>
                    <h2>Novo anuncio</h2>

                    <form onSubmit={this.handleSubmit}>
                        <div className='form-group'>
                            <label htmlFor='foto'>Foto</label>
                            <input type='file' className='form-control' id='foto' ref={(ref) => this.foto = ref} />
                        </div>

                        <div className='form-group'>
                            <label htmlFor='nome'>Categoria</label>

                            <select ref={(ref) => this.categoria = ref}>
                                {Object.keys(this.props.categorias).map((key, i) => {
                                    const res = this.props.categorias[key]

                                    return (
                                        <option key={'catAtual-' + i} value={res.url}>{res.categoria}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className='form-group'>
                            <label htmlFor='nome'>Nome</label>
                            <input type='text' className='form-control' id='nome' ref={(ref) => this.nome = ref} />
                        </div>

                        <div className='form-group'>
                            <label htmlFor='descricao'>Descrição</label>
                            <input type='text' className='form-control' id='descricao' ref={(ref) => this.descricao = ref} />
                        </div>

                        <div className='form-group'>
                            <label htmlFor='preco'>Preço</label>
                            <input type='text' className='form-control' id='preco' ref={(ref) => this.preco = ref} />
                        </div>

                        <div className='form-group'>
                            <label htmlFor='telefone'>Telefone</label>
                            <input type='text' className='form-control' id='telefone' ref={(ref) => this.telefone = ref} />
                        </div>

                        <div className='form-group'>
                            <label htmlFor='vendedor'>Vendedor</label>
                            <input type='text' className='form-control' id='vendedor' ref={(ref) => this.vendedor = ref} />
                        </div>

                        <button type='submit' className='btn btn-primary'>Salvar</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default NovoAnuncio