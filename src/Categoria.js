import React, { Component } from 'react'
import axios from 'axios'
import AnuncioHome from './AnuncioHome'

class Categoria extends Component {
    constructor(props) {
        super(props)

        this.state = {
            anuncios: {},
            isLoading: false
        }

        this.loadAnuncios = this.loadAnuncios.bind(this)

        this.loadAnuncios(this.props.match.params.urlCat);
    }

    loadAnuncios(urlCategoria) {
        this.setState({ anuncios: {}, isLoading: true })

        axios.get('https://mercadodev-50271.firebaseio.com/anuncios.json?orderBy="categoria"&equalTo="' + urlCategoria + '"').then((res) => {
            this.setState({ anuncios: res['data'], isLoading: false })
            this.categoria = urlCategoria
        })
    }

    componentWillReceiveProps(newProps) {
        if (newProps.match.params.urlCat) {
            if (newProps.match.params.urlCat != this.categoria) {
                this.loadAnuncios(newProps.match.params.urlCat)
            }
        }
    }

    render() {
        let indexA = 0

        return (
            <div>
                <h2>Categoria atual: {this.props.match.params.urlCat}</h2>

                {this.state.isLoading && <i className='fa fa-circle-o-notch fa-spin fa-3x fa-fw'></i>}

                {
                    !this.state.isLoading && Object.keys(this.state.anuncios).length == 0 && <p>Nenhum registro</p>
                }

                <div className="row">
                    {Object.keys(this.state.anuncios).map((key, i) => {
                        const res = this.state.anuncios[key]

                        return [
                            <AnuncioHome key={'anu-' + i} anuncio={res} id={key} />,
                            ++indexA % 4 === 0 && <div key={'anu-div-' + i} className="w-100"></div>
                        ]
                    }
                    )}
                </div>
            </div>
        )
    }
}

export default Categoria