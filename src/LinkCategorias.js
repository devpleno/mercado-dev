import React from 'react'
import { Link } from 'react-router-dom'

const LinkCategorias = ({ categoria, icone }) => {
    return (
        <Link to={`/categorias/` + categoria.url} className="btn btn-secondary h-100 m-2 col-sm">
            <i className={`fa  fa-4x ` + icone} aria-hidden="true"></i><br />
            {categoria.titulo}
        </Link>
    )
}

export default LinkCategorias